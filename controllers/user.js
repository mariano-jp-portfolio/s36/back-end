const User = require('../models/user');
const Course = require('../models/course');
const bcrypt = require('bcryptjs');
const { OAuth2Client } = require('google-auth-library');
const auth = require('../auth');

const clientId = '1069368278823-o46qggfgrnknd3mmhmulcmr0ds8ce7pq.apps.googleusercontent.com';

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10),
		loginType: 'email'
	})

	return user.save().then((user, err) => {
		return (err) ? false : true
	})
}

module.exports.login = (params) => {
	return User.findOne({ email: params.email }).then(user => {
		if (user === null) { 
			return { error: 'does-not-exist'};
		}
		
		if (user.loginType !== 'email') {
			return { error: 'login-type-error'};
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

		if (isPasswordMatched) {
			return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return { error: 'incorrect-password'};
		}
	})
}

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	return User.findById(params.userId).then(user => {
		user.enrollments.push({ courseId: params.courseId })

		return user.save().then((user, err) => {
			return Course.findById(params.courseId).then(course => {
				course.enrollees.push({ userId: params.userId })

				return course.save().then((course, err) => {
					return (err) ? false : true
				})
			})
		})
	})
}

module.exports.updateDetails = (params) => {
	
}

module.exports.changePassword = (params) => {
	
}

// We use async and await keyword if functions return a promise
// Promises are commonly returned if we are working with databases
module.exports.verifyGoogleTokenId = async (tokenId) => {
	const client = new OAuth2Client(clientId); //OAuth2Client serves as the middleman between our backend and Google's DB
	
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	});
	
	// console.log(data);
	
	// If user's email is verified by Google, look it up in our DB
	// If user is registered, proceed with login
	// else, register them first
	if (data.payload.email_verified === true) {
		const user = await User.findOne({ email: data.payload.email });
		// console.log(user);
		
		if (user !== null) {
				if (user.loginType === 'google') {
					return {accessToken: auth.createAccessToken(user.toObject())}
				} else {
					return {error: "login-type-error"}
				}
		} else {
			let user = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})

			return user.save().then((user, err) => {
				// toObject() is a mongoose method that returns plain javascript object
				return {accessToken: auth.createAccessToken(user.toObject())}
			})
		}
	} else {
		return { error: 'google-auth-error'}
	}
};